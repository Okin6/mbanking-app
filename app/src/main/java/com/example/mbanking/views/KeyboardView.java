package com.example.mbanking.views;

import android.content.Context;
import android.text.Editable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.mbanking.R;
import com.google.android.material.textfield.TextInputLayout;

import androidx.annotation.IdRes;

public class KeyboardView extends FrameLayout implements View.OnClickListener {

    private EditText passField;
    private TextInputLayout passFieldLayout;

    public KeyboardView(Context context) {
        super(context);
        init();
    }

    public KeyboardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public KeyboardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.keyboard_view, this);
        initViews();
    }

    private void initViews() {
        passField = $(R.id.passField);
        passFieldLayout = $(R.id.passFieldLayout);
        $(R.id.key_0).setOnClickListener(this);
        $(R.id.key_1).setOnClickListener(this);
        $(R.id.key_2).setOnClickListener(this);
        $(R.id.key_3).setOnClickListener(this);
        $(R.id.key_4).setOnClickListener(this);
        $(R.id.key_5).setOnClickListener(this);
        $(R.id.key_6).setOnClickListener(this);
        $(R.id.key_7).setOnClickListener(this);
        $(R.id.key_8).setOnClickListener(this);
        $(R.id.key_9).setOnClickListener(this);
        $(R.id.key_ok).setOnClickListener(this);
        $(R.id.key_backspace).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        passFieldLayout.setError(null);

        if (v.getTag() != null && "number_button".equals(v.getTag())) {
            passField.append(((TextView) v).getText());
            return;
        }
        switch (v.getId()) {
            case R.id.key_backspace: {
                Editable editable = passField.getText();
                int charCount = editable.length();
                if (charCount > 0) {
                    editable.delete(charCount - 1, charCount);
                }
            }
            break;
        }
    }

    public String getInputText() {
        return passField.getText().toString();
    }

    protected <T extends View> T $(@IdRes int id) {
        return (T) super.findViewById(id);
    }
}