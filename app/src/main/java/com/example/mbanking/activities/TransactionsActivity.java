package com.example.mbanking.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;

import com.example.mbanking.R;
import com.example.mbanking.adapters.TransactionsAdapter;
import com.example.mbanking.databinding.ActivityTransactionsBinding;
import com.example.mbanking.models.Account;
import com.example.mbanking.models.User;
import com.example.mbanking.utils.EncryptorInfo;
import com.example.mbanking.viewmodels.TransactionsViewModel;
import com.example.mbanking.views.DialogView;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.io.IOException;
import java.security.GeneralSecurityException;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class TransactionsActivity extends AppCompatActivity {

    private TransactionsViewModel transactionsViewModel;
    private User user;
    private RecyclerView recyclerView;
    private TransactionsAdapter transactionsAdapter;
    private BottomNavigationView bnv;
    private ActivityTransactionsBinding binding;
    private Intent intent;
    private RelativeLayout progressBar;
    private EncryptorInfo encryptorInfo;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this,R.layout.activity_transactions);

        recyclerView = findViewById(R.id.transactions);
        bnv = findViewById(R.id.bottom_navigation);
        progressBar = findViewById(R.id.loadingPanel);

        progressBar.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        //IZVLACENJE PODATAKA IZ SHARED PREFERENCES
        try {
            encryptorInfo = new EncryptorInfo(this);
            user = new User();
            user.setName(encryptorInfo.getName());
            user.setSurname(encryptorInfo.getSurname());
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        binding.setUser(user);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        transactionsAdapter = new TransactionsAdapter();
        recyclerView.setAdapter(transactionsAdapter);

        transactionsViewModel = new ViewModelProvider(this).get(TransactionsViewModel.class);
        binding.setViewmodel(transactionsViewModel);

        transactionsViewModel.observeUser().observe(this, new Observer<User>() {
            @Override
            public void onChanged(User gottenUser) {
                binding.setAccount(gottenUser.getAccounts().get(0));
                binding.getUser().setAccounts(gottenUser.getAccounts());
                binding.getUser().setId(gottenUser.getId());
                transactionsAdapter.setTransactions(gottenUser.getAccounts().get(0).getTransactions());

                progressBar.setVisibility(View.GONE);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
        });

        bnv.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                        //SLAGANJE RACUNA U DIALOG
                        ArrayAdapter<Account> arrayAdapter = new ArrayAdapter<Account>(TransactionsActivity.this, android.R.layout.select_dialog_singlechoice);
                        AlertDialog.Builder builder;
                        for(Account account : binding.getUser().getAccounts()){
                            arrayAdapter.add(account);
                        }

                        switch (item.getItemId()) {
                            case R.id.accounts_icon:
                                builder = new AlertDialog.Builder(TransactionsActivity.this);
                                builder.setTitle("Za koji racun zelite vidjeti podatke?");
                                builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface d, int n) {
                                        setAccountDetails(arrayAdapter.getItem(n));
                                    }

                                });
                                builder.setNegativeButton("Odustani", null);
                                builder.show();
                                break;

                            case R.id.logout_icon:
                                DialogView alert = new DialogView();
                                alert.showDialog(TransactionsActivity.this);
                        }

                        return true;

                    }
                });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setupIntent();
        startActivity(intent);
        TransactionsActivity.this.finish();
    }

    public void sortDate(View v){
        transactionsAdapter.sortByDate();
    }

    public void sortAmount(View v){
        transactionsAdapter.sortByAmount();
    }

    public void sortDescription(View v){
        transactionsAdapter.sortByDescription();
    }

    private void setAccountDetails (Account account){
        binding.setAccount(account);
        transactionsAdapter.setTransactions(account.getTransactions());
    }

    private void setupIntent(){
        intent = new Intent(TransactionsActivity.this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
    }
}
