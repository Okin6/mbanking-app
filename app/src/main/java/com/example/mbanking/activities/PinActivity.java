package com.example.mbanking.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.mbanking.utils.EncryptorInfo;
import com.example.mbanking.views.KeyboardView;
import com.example.mbanking.R;
import com.google.android.material.textfield.TextInputLayout;

import java.io.IOException;
import java.security.GeneralSecurityException;

import androidx.appcompat.app.AppCompatActivity;

public class PinActivity extends AppCompatActivity {

    private String pass;
    private KeyboardView keyboardView;
    private TextView okButton;
    private TextInputLayout passFieldLayout;
    private Intent intent;
    private EncryptorInfo encryptorInfo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin);

        //POSTAVLJANJE SHARED PREFERENCES-A
        try {
            encryptorInfo = new EncryptorInfo(this);
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        keyboardView = findViewById(R.id.keyboard);
        okButton = keyboardView.findViewById(R.id.key_ok);
        passFieldLayout = findViewById(R.id.passFieldLayout);

        passFieldLayout.setErrorEnabled(true);

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //PROVJERA PINA

                pass = keyboardView.getInputText();
                //PROVJERA JE LI PIN UPISAN
                boolean pin = encryptorInfo.checkPinExists();

                if(pass.length()<4){
                    passFieldLayout.setError("PIN mora sadržavati najmanje 4 znaka!");
                }

                else if(pin){
                    //PROVJERA JE LI PIN ODGOVARA ONOM U SHARED PREFERENCES
                    if(encryptorInfo.checkPin(pass)){
                        setupIntent();
                        PinActivity.this.startActivity(intent);
                        PinActivity.this.finish();
                    }
                    else{
                        passFieldLayout.setError("Krivi PIN");
                    }
                }
                else{
                    //POSTAVLJANJE PIN-A
                    encryptorInfo.setPin(pass);
                    setupIntent();
                    PinActivity.this.startActivity(intent);
                    PinActivity.this.finish();
                }
            }
        });
    }

    private void setupIntent(){
        intent = new Intent(PinActivity.this, TransactionsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
    }
}
