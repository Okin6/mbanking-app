package com.example.mbanking.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.mbanking.R;
import com.example.mbanking.models.User;
import com.example.mbanking.utils.EncryptorInfo;

import java.io.IOException;
import java.security.GeneralSecurityException;

public class RegistrationActivity extends AppCompatActivity {

    private EditText name, surname;
    private Button registerButton;
    private Intent intent;
    private EncryptorInfo encryptorInfo;
    private Boolean isRegistered;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        //PREGLED ENCRYPTEDSHAREDPREFERENCES-A KAKO BI VIDJELI JE LI KORISNIK REGISTRIRAN
        try {
            encryptorInfo = new EncryptorInfo(this);
            isRegistered = encryptorInfo.checkRegistered();
            if(isRegistered){
                intent = new Intent(RegistrationActivity.this,LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                RegistrationActivity.this.startActivity(intent);
                this.finish();
            }
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        registerButton = findViewById(R.id.submit);
        name = findViewById(R.id.nameText);
        surname = findViewById(R.id.surnameText);

        name.addTextChangedListener(loginTextWatcher);
        surname.addTextChangedListener(loginTextWatcher);

        registerButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                User user = new User();
                user.setName(name.getText().toString());
                user.setSurname(surname.getText().toString());

                // REGISTRIRANJE KORISNIKA U SHARED PREFERENCES
                encryptorInfo.setRegisterInfo(user.getName(),user.getSurname());

                intent = new Intent(RegistrationActivity.this,PinActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                RegistrationActivity.this.startActivity(intent);
                RegistrationActivity.this.finish();

            }
        });
    }

    //ONEMOGUCAVANJE REGISTRACIJE BEZ UPISIVANJA IMENA I PREZIMENA

    private TextWatcher loginTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            String nameInput = name.getText().toString().trim();
            String surnameInput = surname.getText().toString().trim();

            registerButton.setEnabled(!nameInput.isEmpty() && !surnameInput.isEmpty());
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };
}
