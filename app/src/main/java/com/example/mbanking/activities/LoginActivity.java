package com.example.mbanking.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.mbanking.R;
import com.example.mbanking.databinding.ActivityLoginBinding;
import com.example.mbanking.models.User;
import com.example.mbanking.utils.EncryptorInfo;

import java.io.IOException;
import java.security.GeneralSecurityException;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

public class LoginActivity extends AppCompatActivity {
    private User user;
    private Intent intent;
    private EncryptorInfo encryptorInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityLoginBinding binding = DataBindingUtil.setContentView(this,R.layout.activity_login);

        //IZVLACENJE PODATAKA IZ SHARED PREFERENCES-A
        try {
            encryptorInfo = new EncryptorInfo(this);
            user = new User();
            user.setName(encryptorInfo.getName());
            user.setSurname(encryptorInfo.getSurname());
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        binding.setUser(user);
    }

    public void login(View v){
        intent = new Intent(LoginActivity.this,PinActivity.class);
        LoginActivity.this.startActivity(intent);
    }
}
