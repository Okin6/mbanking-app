package com.example.mbanking.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.mbanking.R;
import com.example.mbanking.databinding.TransactionViewBinding;
import com.example.mbanking.models.Transaction;
import com.example.mbanking.utils.Sorter;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class TransactionsAdapter extends RecyclerView.Adapter<TransactionsAdapter.TransactionHolder> {

    private List<Transaction> transactions = new ArrayList<>();
    private Sorter sorter = new Sorter();
    private boolean descriptionAscending = false;
    private boolean dateAscending = false;
    private boolean amountAscending = false;

    @NonNull
    @Override
    public TransactionHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        TransactionViewBinding transactionViewBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.transaction_view,parent,false);
        return new TransactionHolder(transactionViewBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull TransactionHolder holder, int position) {
        Transaction currentTransaction = transactions.get(position);
        holder.transactionViewBinding.setTransaction(currentTransaction);
    }

    @Override
    public int getItemCount() {
        return transactions.size();
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
        notifyDataSetChanged();
    }

    class TransactionHolder extends RecyclerView.ViewHolder{
        private TransactionViewBinding transactionViewBinding;

        public TransactionHolder(@NonNull TransactionViewBinding transactionViewBinding) {
            super(transactionViewBinding.getRoot());
            this.transactionViewBinding = transactionViewBinding;
        }

    }


    //SORTIRANJA

    public void sortByDate(){
        if(dateAscending){
            this.transactions = sorter.sortDateDescending(this.transactions);
            dateAscending = false;
            notifyDataSetChanged();
        }
        else{
            this.transactions = sorter.sortDateAscending(this.transactions);
            dateAscending = true;
            notifyDataSetChanged();
        }
    }

    public void sortByDescription(){
        if(descriptionAscending){
            this.transactions = sorter.sortDescriptionDescending(this.transactions);
            descriptionAscending = false;
            notifyDataSetChanged();
        }
        else {
            this.transactions = sorter.sortDescriptionAscending(this.transactions);
            descriptionAscending = true;
            notifyDataSetChanged();
        }

    }

    public void sortByAmount(){
        if(amountAscending){
            this.transactions = sorter.sortAmountDescending(this.transactions);
            amountAscending = false;
            notifyDataSetChanged();
        }
        else{
            this.transactions = sorter.sortAmountAscending(this.transactions);
            amountAscending = true;
            notifyDataSetChanged();
        }
    }

}
