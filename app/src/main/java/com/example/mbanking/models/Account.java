package com.example.mbanking.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import androidx.annotation.NonNull;

public class Account {
    @SerializedName("id")
    private String id;
    @SerializedName("IBAN")
    private String iban;
    @SerializedName("amount")
    private String amount;
    @SerializedName("currency")
    private String currency;
    @SerializedName("transactions")
    private List<Transaction> transactions;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getIban() {
        return iban;
    }

    public String getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    @NonNull
    @Override
    public String toString() {
        return "Racun " + this.id;
    }
}
