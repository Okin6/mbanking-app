package com.example.mbanking.repository;

import android.app.Application;

import com.example.mbanking.models.User;

import hu.akarnokd.rxjava3.retrofit.RxJava3CallAdapterFactory;
import io.reactivex.rxjava3.core.Single;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TransactionsRepository {

    private TransactionsReFit transactionsReFit;
    private Single<User> user;


    public TransactionsRepository(Application application){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://mobile.asseco-see.hr/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                .build();
        transactionsReFit = retrofit.create(TransactionsReFit.class);
    }

    public Single<User> getUser(){
        user = transactionsReFit.getUser();
        return user;
    }
}
