package com.example.mbanking.repository;

import com.example.mbanking.models.User;

import io.reactivex.rxjava3.core.Single;
import retrofit2.http.GET;

public interface TransactionsReFit {
    @GET("/builds/ISBD_public/Zadatak_1.json")
    Single<User> getUser();
}
