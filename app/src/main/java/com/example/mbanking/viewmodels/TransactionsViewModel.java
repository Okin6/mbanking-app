package com.example.mbanking.viewmodels;

import android.app.Application;

import com.example.mbanking.models.User;
import com.example.mbanking.repository.TransactionsRepository;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class TransactionsViewModel extends AndroidViewModel {

    private TransactionsRepository transactionsRepository;
    private MutableLiveData<User> mutableUser = new MutableLiveData<>();


    public TransactionsViewModel(@NonNull Application application) {
        super(application);
        transactionsRepository = new TransactionsRepository(application);
        getUser();
    }



    private void getUser(){
        Single<User> user = transactionsRepository.getUser();
        user.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<User>() {
                    @Override
                    public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {

                    }

                    @Override
                    public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull User gottenUser) {
                        mutableUser.setValue(gottenUser);

                    }

                    @Override
                    public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {

                    }
                });

    }
    public LiveData<User> observeUser(){
        return mutableUser;
    }
}
