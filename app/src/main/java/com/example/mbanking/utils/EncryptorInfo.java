package com.example.mbanking.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Objects;

import androidx.security.crypto.EncryptedSharedPreferences;
import androidx.security.crypto.MasterKey;

public class EncryptorInfo {

    private SharedPreferences sharedPreferences;
    private MasterKey masterKey;
    private Context context;

    private void setPrefs() throws GeneralSecurityException, IOException {
        sharedPreferences = EncryptedSharedPreferences.create(
                context,
                "shared",
                masterKey,
                EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM);
    }

    public EncryptorInfo(Activity activity) throws GeneralSecurityException, IOException {
        this.context = activity.getApplicationContext();
        KeyGenParameterSpec spec = new KeyGenParameterSpec.Builder(
                "_androidx_security_master_key_",
                KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
                .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
                .setKeySize(256)
                .build();

        masterKey = new MasterKey.Builder(activity)
                .setKeyGenParameterSpec(spec)
                .build();

        setPrefs();
    }

    public Boolean checkRegistered() {
        return Objects.equals(sharedPreferences.getString("registered", "No"), "Yes");
    }

    public void setRegisterInfo(String u, String p){
        sharedPreferences.edit()
                .putString("u",u)
                .putString("p",p)
                .putString("registered","Yes")
                .apply();
    }

    public String getName(){
        return sharedPreferences.getString("u","");
    }
    public String getSurname(){
        return sharedPreferences.getString("p","");
    }

    public void setPin(String pin){
        sharedPreferences.edit()
                .putString("pin",pin)
                .apply();
    }

    public boolean checkPinExists(){
        return !Objects.equals(sharedPreferences.getString("pin", ""), "");
    }

    public boolean checkPin(String pass){
        return Objects.equals(sharedPreferences.getString("pin", ""), pass);
    }

}
