package com.example.mbanking.utils;

import com.example.mbanking.models.Transaction;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Sorter {

    public List<Transaction> sortDateAscending(List<Transaction> transactions){
        Collections.sort(transactions, new Comparator<Transaction>() {
            public int compare(Transaction t1, Transaction t2) {
                Date d1 = null,d2 = null;
                try {
                    d1 = formatDate(t1);
                    d2 = formatDate(t2);
                }
                catch (ParseException e) {
                    e.printStackTrace();
                }
                return d1.compareTo(d2);
            }
        });
        return transactions;
    }

    public List<Transaction> sortDateDescending(List<Transaction> transactions) {
        Collections.sort(transactions, new Comparator<Transaction>() {
            public int compare(Transaction t1, Transaction t2) {
                Date d1 = null,d2 = null;
                try {
                    d1 = formatDate(t1);
                    d2 = formatDate(t2);
                }
                catch (ParseException e) {
                    e.printStackTrace();
                }
                return d2.compareTo(d1);
            }
        });
        return transactions;
    }

    private Date formatDate(Transaction transaction) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        Date date = (Date)formatter.parse(transaction.getDate());
        return date;
    }

    public List<Transaction> sortDescriptionAscending(List<Transaction> transactions){
        Collections.sort(transactions, new Comparator<Transaction>() {
            @Override
            public int compare(final Transaction object1, final Transaction object2) {
                return object1.getDescription().compareTo(object2.getDescription());
            }
        });
        return transactions;
    }

    public List<Transaction> sortDescriptionDescending(List<Transaction> transactions){
        Collections.sort(transactions, new Comparator<Transaction>() {
            @Override
            public int compare(final Transaction object1, final Transaction object2) {
                return object2.getDescription().compareTo(object1.getDescription());
            }
        });
        return transactions;
    }

    public List<Transaction> sortAmountAscending(List<Transaction> transactions){
        Collections.sort(transactions, new Comparator<Transaction>() {
            @Override
            public int compare(final Transaction object1, final Transaction object2) {
                String a1 = object1.getAmount().substring(0, object1.getAmount().length() - 4);
                String a2 = object2.getAmount().substring(0, object1.getAmount().length() - 4);
                NumberFormat nf = NumberFormat.getInstance(Locale.FRANCE);
                Number n1,n2;
                try {
                    n1 = nf.parse(a1);
                    n2 = nf.parse(a2);
                    Float f1 = n1.floatValue();
                    Float f2 = n2.floatValue();
                    return f1.compareTo(f2);
                } catch (ParseException e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        });
        return transactions;
    }

    public List<Transaction> sortAmountDescending(List<Transaction> transactions){
        Collections.sort(transactions, new Comparator<Transaction>() {
            @Override
            public int compare(final Transaction object1, final Transaction object2) {
                String a1 = object1.getAmount().substring(0, object1.getAmount().length() - 4);
                String a2 = object2.getAmount().substring(0, object1.getAmount().length() - 4);
                NumberFormat nf = NumberFormat.getInstance(Locale.FRANCE);
                Number n1,n2;
                try {
                    n1 = nf.parse(a1);
                    n2 = nf.parse(a2);
                    Float f1 = n1.floatValue();
                    Float f2 = n2.floatValue();
                    return f2.compareTo(f1);
                } catch (ParseException e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        });
        return transactions;
    }
}
